var bike1ID = -110955803;
var bike2ID = -110950709;

var infoTweets = [
    "Did you know that Canada recommends at least 150 minutes of moderate to vigorous physical activity for every adult?",
    "Reducing sedentary behavior reduces your risk of: heart disease, stroke, high blood pressure and Type 2 Diabetes.",
    "Do a moderate to vigorous physical activity like walking, jogging, or biking at our active bike workstations!",
    "Sedentary lifestyles are responsible for an estimated $24 billion in direct medical spending.",
    "300,00 deaths occur annually due to inactivity and poor dietary habits in the United States alone.",
    "The average Canadian spends close to 10 hours per day being sedentary, according to Statistics Canada."
];

var promoTweets = [
    "Come check out the bike workstations located at the 3rd floor Commons Area of the Irving K. Barber Learning Center!",
    "Study and exercise at the same time! Come by the Irving K. Barber Learning Center and use an active workstation!",
    "Don't have the time to exercise? Come to the Irving K. Barber Learning Center and get your work done at our bike workstation!"
];

var prevPromo = 10;
var prevInfo = 10;

var dateFormat = require('dateformat');

var Twitter = require('twitter');

//Database Functionality
var mysql = require('mysql');

//Connect to the Database
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "team41",
    database: "active_workstation"
});

//Connect to the Database
con.connect(function(err) {
    if (err) {
        console.log("An error occurred while attempting to connect to the database!")
        console.log(err)
    }
    console.log("Connected to database: active_workstation!");
});

// Check if environment variables are set
if(!process.env.CONSUMER_KEY || !process.env.CONSUMER_SECRET ||
	!process.env.ACCESS_KEY || !process.env.ACCESS_SECRET)
{
    var env = require('./env.js'); // JS file for setting environment variables
	console.log('Environment variables set.')
}

// Define a new Twitter client
var tweetClient = new Twitter({
	consumer_key: process.env.CONSUMER_KEY,
	consumer_secret: process.env.CONSUMER_SECRET,
	access_token_key: process.env.ACCESS_KEY,
	access_token_secret: process.env.ACCESS_SECRET
});

var bikeOne = {
    totalCals: 0,
    totalDist: 0,
    totalTime: 0,
    MonthCals: 0,
    MonthDist: 0
}

var bikeTwo = {
    totalCals: 0,
    totalDist: 0,
    totalTime: 0,
    MonthCals: 0,
    MonthDist: 0
}

var minutes = 2;
var debugMode = 0;

if(debugMode == 0)
{
    var totalCals = 0;
    var totalDist = 0;

    findUserInDatabase(bike1ID, function(err,data){
        console.log(data);
        bikeOne.totalCals = data[0].TotalCalories;
        bikeOne.totalDist = data[0].TotalDistance;
        bikeOne.totalTime = data[0].TotalTime;
        bikeOne.MonthCals = data[0].MonthCalories;
        bikeOne.MonthDist = data[0].MonthDistance;
    });

    findUserInDatabase(bike2ID, function(err,data){
        bikeTwo.totalCals = data[0].TotalCalories;
        bikeTwo.totalDist = data[0].TotalDistance;
        bikeTwo.totalTime = data[0].TotalTime;
        bikeTwo.MonthCals = data[0].MonthCalories;
        bikeTwo.MonthDist = data[0].MonthDistance;
    });

    var intervalA = setInterval(function(){
        var tweetType = Math.floor(Math.random() * 3) + 1;

        console.log('Type: ' + tweetType);
        if(tweetType == 1)  // Stat tweet
        {
            var statType = Math.floor(Math.random() * 2) + 1;
            updateStatus(tweetType, statType);
        }
        else if(tweetType == 2) // Promo tweet
        {
            var statType = Math.floor(Math.random() * 2);
		if(prevPromo != statType)
		{
            updateStatus(tweetType, statType);
		prevPromo = statType;
		}
        }
        else if(tweetType == 3)
        {
            var statType = Math.floor(Math.random() * 5);
        if(prevInfo != statType){    
	updateStatus(tweetType, statType);
		prevInfo = statType;
}
        }
    }, ((minutes * 60) * 1000));
}
else if(debugMode == 1)
{
    var intervalA = setInterval(function(){
        var totalCals = Math.floor((Math.random() * 50) + 1);
        var totalDist = Math.floor((Math.random() * 1000) + 1);
        var tweetType = (Math.random() <= 0.5) ? 1 : 2;


        console.log('Type: ' + tweetType);
        if(tweetType == 1)
        {
            updateStatus(1, totalCals);
        }
        else if(tweetType == 2)
        {
            updateStatus(2, totalDist);
        }
    }, ((minutes * 60) * 1000));
}


/** A function to query the database to find a specified user based on the indentifier. This function will be used when attempting to login
 * Parameter: studentID = the identifier for a user
 * Return: -1 = If the user does not exist in the database
 * Return: rows = The row of data for the specified user (The user was successfully found in the database)
 */
function findUserInDatabase(studentID, findUserCallBack) {
    con.query('SELECT * FROM stats WHERE StudentID ='+ studentID, function(err, rows) {
        if(err) {
            findUserCallBack(err, null);
        }
        else {
            findUserCallBack(null, rows);
        }
    });
}

/**
 * updateStatus()
 * Updates the twitter feed with relevant tweets relating to the active workstation
 * The tweets are sent in intervals.
 * 
 */
function updateStatus(tweetType, statType){

    var dateNow = dateFormat(new Date(), "ddd, mmm dS, yyyy, h:MM:ss TT");

    if(tweetType == 1 && statType == 1)
    {
        tweetClient.post('statuses/update',
            {status: dateNow +': ' + 'Active Workstation users have now burned ' + (bikeOne.totalCals + bikeTwo.totalCals) + ' calories! Keep it up, students!'},
            function(error, tweet, response){
                if(error)
                {
                    console.log(error);
                    //throw error; // Quit the program when twitter error occurs
                }
                //console.log(tweet);
                //console.log(response);
            });
    }
    else if(tweetType == 1 && statType == 2)
    {
        tweetClient.post('statuses/update',
            {status: dateNow + ': ' + 'Students have now pedalled for a total distance of ' + (bikeOne.totalDist + bikeTwo.totalDist) + ' m! Stay healthy out there!'},
            function(error, tweet, response){
                if(error)
                {
                    console.log(error);
                    //throw error; // Quit the program when twitter error occurs
                }
                //console.log(tweet);
                //console.log(response);
            });
    }
    else if(tweetType == 2)
    {
        tweetClient.post('statuses/update',
            {status: dateNow + ': ' + promoTweets[statType]},
            function(error, tweet, response){
                if(error)
                {
                    console.log(error);
                    //throw error; // Quit the program when twitter error occurs
                }
                //console.log(tweet);
                //console.log(response);
            });
    }
    else if(tweetType == 3)
    {
        tweetClient.post('statuses/update',
            {status: dateNow + ': ' + infoTweets[statType]},
            function(error, tweet, response){
                if(error)
                {
                    console.log(error);
                    //throw error; // Quit the program when twitter error occurs
                }
                //console.log(tweet);
                //console.log(response);
            });
    }

}
