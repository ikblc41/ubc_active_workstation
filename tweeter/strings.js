// Statistics tweets
const s1a = 'UBC Students have pedalled for a total of '
const s1b = ' km this year! Come by the IKBLC and check out the new Active Wokrstation!'
const s2a = 'UBC Students are pedalling at an average speed of '
const s2b = ' km/hr. A decent pace for staying active while studying!'

// Marketing tweets
const m1 = 'UBC Students! Come check out the active workstations located at the nth floor of the Irving K. Barber Learning Center!'
const m2 = 'Study and exercise at the same time! Come by the Irving K. Barber Learning Center and use an active workstation!'

// Sedentary information tweets
const i1 = 'Sitting and studying for long durations increase your chances for heart failure.'
const i2 = 'The Govt of Canada recommends at least 60 mins of daily physical activity in order to lead a healthy lifestyle.'