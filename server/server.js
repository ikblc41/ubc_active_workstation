var express = require('express')
var app = express()
var http = require('http')
var https = require('https')
var path = require('path')
var fs = require('fs')


// create the HTTP server
var serverHTTP = http.createServer(app)
var io = require('socket.io')(serverHTTP)

serverHTTP.listen(3000, function () {
    console.log('HTTP listening on *=3000')
})

// create the HTTPS server

//var ssl = {
//	key: fs.readFileSync('/etc/letsencrypt/live/ubcactiveworkstation.ca/privkey.pem'),
//	cert: fs.readFileSync('/etc/letsencrypt/live/ubcactiveworkstation.ca/fullchain.pem')
//}
//var serverHTTPS = https.createServer(ssl, app)
//var secure_io = require('socket.io')(serverHTTPS)


// var ssl = {
// 	key: fs.readFileSync('/etc/letsencrypt/live/ubcactiveworkstation.ca/privkey.pem'),
// 	cert: fs.readFileSync('/etc/letsencrypt/live/ubcactiveworkstation.ca/fullchain.pem')
// }
// var serverHTTPS = https.createServer(ssl, app)
// var secure_io = require('socket.io')(serverHTTPS)
//

// serverHTTPS.listen(3443, function() {
// 	console.log('HTTPS listening on *= 3443')
// })

//Database Functionality
var mysql = require('mysql');

//Connect to the Database 
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "team41",
    database: "active_workstation"
});

//Connect to the Database 
con.connect(function (err) {
    if (err) {
        console.log("An error occurred while attempting to connect to the database!")
        console.log(err)
    }
    console.log("Connected to database: active_workstation!");
});


// Timeout Variables
var bikeOneTimeout = 0;
var bikeTwoTimeout = 0;

// Distance Accumulator Variables
var bikeOnePrevDistance = 0;
var bikeTwoPrevDistance = 0;
var bikeOneDailyDistance = 0;
var bikeTwoDailyDistance = 0;
var bikeOneMonthlyDistance = 0;
var bikeTwoMonthlyDistance = 0;
var prevDay = 10;
var bikeOneDDUpdateFlag = 0;
var bikeTwoDDUpdateFlag = 0;

// Monthly stats logic
var startFlag = 0;
var month;
var date;
var year;
const SEPTEMBER = 8;    // January is 0, September is 8

const timeoutMaxMillis = 300000; // 5 minutes timeout duration

//User Variables
const USERLOGINLOCK = -2; //Flag to lock bike logins

// Bike data variables
var bikePrevTime1 = 0.0;	// Time of previous reading for bike 1
var bikePrevTime2 = 0.0;    // Time of previous reading for bike 2
var bikeGearRatio = 2.125;	// Front gear rotates 2.3 times compared to rear gear
var bikeCircumference = 2.130;    // Circumference of a standard road bike wheel
var timeDiff1 = 0.0;    // Time difference for bike 1
var timeDiff2 = 0.0;    // Time difference for bike 2
var MET = 3.5;
var avgWeight = 65;

var bike1Data = {
    deviceID: 21403,
    rpm: 0,
    distance: 0,
    speed: 0,
    calories: 0,
    sessionStart: 0,
    sessionTime: 0,
    databaseTotalCalories: 0,
    databaseTotalDistance: 0,
    databaseTotalTime: 0,
    databaseMonthCalories: 0,
    databaseMonthDistance: 0,
    databaseMonthTime: 0,
    useTime_u30: 0,
    useTime30_60: 0,
    useTime60_120: 0,
    useTime_o120: 0,
};

var bike2Data = {
    deviceID: 5555,
    rpm: 0,
    distance: 0,
    speed: 0,
    calories: 0,
    sessionStart: 0,
    sessionTime: 0,
    databaseTotalCalories: 0,
    databaseTotalDistance: 0,
    databaseTotalTime: 0,
    databaseMonthCalories: 0,
    databaseMonthDistance: 0,
    databaseMonthTime: 0,
    useTime_u30: 0,
    useTime30_60: 0,
    useTime60_120: 0,
    useTime_o120: 0,
};

var bikeOneCurrUser = {
    studentIdentifier: USERLOGINLOCK,
    totalCaloriesDatabase: 0,
    totalDistanceDatabase: 0,
    totalTimeDatabase: 0,
    monthCaloriesDatabase: 0,
    monthDistanceDatabase: 0,
    monthTimeDatabase: 0
};

var bikeTwoCurrUser = {
    studentIdentifier: USERLOGINLOCK,
    totalCaloriesDatabase: 0,
    totalDistanceDatabase: 0,
    totalTimeDatabase: 0,
    monthCaloriesDatabase: 0,
    monthDistanceDatabase: 0,
    monthTimeDatabase: 0
};

// bike IDs
var bike1ID = -110955803;
var bike2ID = -110950709;

// Try different routing
app.use(express.static(__dirname + '/public'));
bikePrevTime1 = Date.now();
bikePrevTime2 = Date.now();

// Check for connection events
io.on('connection', function (socket) {
    console.log('User connection created');

    // Initialize the daily distance for each bike, check if needed at intervals of half an hour
    setInterval(function () {
        var dateNow = new Date();
        var currDay = dateNow.getDay();

        if (prevDay != currDay) {
            console.log(prevDay + ' != ' + currDay);

            // Query the database for the Bike daily distance values
            con.query('SELECT TotalDistance, MonthDistance FROM stats WHERE StudentID =' + bike1ID, function (err, rows) {
                if (rows[0] == null) {
                    console.log(rows);
                    console.log("Bike One TotalDistance query failed");
                } else {
                    console.log(rows);
                    bikeOneDailyDistance = Number(rows[0].TotalDistance);
                    bikeOneMonthlyDistance = Number(rows[0].MonthDistance);
                    console.log("Bike One Daily and Monthly Distance init: " + bikeOneDailyDistance + ", " + bikeOneMonthlyDistance);
                }
            });

            con.query('SELECT TotalDistance, MonthDistance FROM stats WHERE StudentID =' + bike2ID, function (err, rows) {
                if (rows[0] == null) {
                    console.log(rows);
                    console.log("Bike Two TotalDistance query failed");
                } else {
                    bikeTwoDailyDistance = Number(rows[0].TotalDistance);
                    bikeTwoMonthlyDistance = Number(rows[0].MonthDistance);
                    console.log("Bike Two Daily and Monthly Distance init: " + bikeTwoDailyDistance + ", " + bikeTwoMonthlyDistance);
                }
            });

            // Set currDay as prevDay
            prevDay = currDay;
        }
    }, (1000));

    // Interval for checking user login status
    setInterval(function () {

        // If RPM = 0 and user is logged into Bike one, start a timeout counter for Bike One
        if ((bike1Data.rpm == 0) && (bikeOneCurrUser.studentIdentifier != USERLOGINLOCK)) {
            // Start the timer
            if (bikeOneTimeout == 0) {
                bikeOneTimeout = Date.now();
                console.log("Bike One Timeout started: " + bikeOneTimeout);
            }

            var timeDiffTimeout1 = Date.now() - bikeOneTimeout;

            // If timer >= 5 minutes, log out the user
            if (((timeDiffTimeout1) >= timeoutMaxMillis) && (bikeOneTimeout != 0)) {
                // Perform user logout here
                console.log("Timeout the bike one session");
                //End session button has been pressed, place the stats into variables
                var bikeOneTimeDiff = Date.now() - bike1Data.sessionStart;

                bikeOneTimeDiff = Math.round(bikeOneTimeDiff / 60000);

                //Update total distance, calories, time
                bikeOneCurrUser.totalCaloriesDatabase = bikeOneCurrUser.totalCaloriesDatabase + bike1Data.calories;
                bikeOneCurrUser.totalDistanceDatabase = bikeOneCurrUser.totalDistanceDatabase + bike1Data.distance;
                bikeOneCurrUser.totalTimeDatabase = bikeOneCurrUser.totalTimeDatabase + bikeOneTimeDiff;

                console.log("the end session total calories is:" + bikeOneCurrUser.totalCaloriesDatabase);

                //Update monthly distance, calories, time
                bikeOneCurrUser.monthCaloriesDatabase = bikeOneCurrUser.monthCaloriesDatabase + bike1Data.calories;
                bikeOneCurrUser.monthDistanceDatabase = bikeOneCurrUser.monthDistanceDatabase + bike1Data.distance;
                bikeOneCurrUser.monthTimeDatabase = bikeOneCurrUser.monthTimeDatabase + bikeOneTimeDiff;

                //Place time, distance, calories into the "LastSessionStats columns"
                var userLastSessionCalories = bike1Data.calories;
                var userLastSessionDistance = bike1Data.distance;

                //Update the database
                con.query('UPDATE stats SET TotalCalories =' + bikeOneCurrUser.totalCaloriesDatabase
                    + ', TotalDistance =' + bikeOneCurrUser.totalDistanceDatabase
                    + ', TotalTime =' + bikeOneCurrUser.totalTimeDatabase
                    + ', LastSessionCalories =' + userLastSessionCalories
                    + ', LastSessionDistance =' + userLastSessionDistance
                    + ', LastSessionTime =' + bikeOneTimeDiff
                    + ', MonthCalories =' + bikeOneCurrUser.monthCaloriesDatabase
                    + ', MonthDistance =' + bikeOneCurrUser.monthDistanceDatabase
                    + ', MonthTime =' + bikeOneCurrUser.monthTimeDatabase
                    + ' WHERE StudentID =' + bikeOneCurrUser.studentIdentifier, function (err, rows) {
                    console.log(err);
                });

                updateDurationOfUse(bikeOneTimeDiff, 1);

                //Set the student identifier back to default value to unlock loggin for other users
                bikeOneCurrUser.studentIdentifier = USERLOGINLOCK;
                bikeOneCurrUser.totalCaloriesDatabase = 0;
                bikeOneCurrUser.totalDistanceDatabase = 0;
                bikeOneCurrUser.totalTimeDatabase = 0;
                bikeOneCurrUser.monthCaloriesDatabase = 0;
                bikeOneCurrUser.monthDistanceDatabase = 0;
                bikeOneCurrUser.monthTimeDatabase = 0;

                //Acknowledge the user session has ended
                io.emit('doneBikeOneSession');

                // Set the timeout to 0 again
                bikeOneTimeout = 0;
                console.log("Bike One timeout duration reached" + timeDiffTimeout1);
            }
        }

        // If RPM = 0 and user is logged into Bike Two, start a timeout counter for Bike Two
        if ((bike2Data.rpm == 0) && (bikeTwoCurrUser.studentIdentifier != USERLOGINLOCK)) {
            // Start the timer
            if (bikeTwoTimeout == 0) {
                bikeTwoTimeout = Date.now();
                console.log("Bike Two Timeout started: " + bikeTwoTimeout);
            }

            var timeDiffTimeout2 = Date.now() - bikeTwoTimeout;

            // If timer >= 2 minutes, log out the user
            if (((timeDiffTimeout2) >= timeoutMaxMillis) && (bikeTwoTimeout != 0)) {
                // Perform user logout here
                console.log("Timeout the bike two session");
                //End session button has been pressed, place the stats into variables
                var bikeTwoTimeDiff = Date.now() - bike2Data.sessionStart;

                bikeTwoTimeDiff = Math.round(bikeTwoTimeDiff / 60000);

                //Update total distance, calories, time
                bikeTwoCurrUser.totalCaloriesDatabase = bikeTwoCurrUser.totalCaloriesDatabase + bike2Data.calories;
                bikeTwoCurrUser.totalDistanceDatabase = bikeTwoCurrUser.totalDistanceDatabase + bike2Data.distance;
                bikeTwoCurrUser.totalTimeDatabase = bikeTwoCurrUser.totalTimeDatabase + bikeTwoTimeDiff;

                //Update monthly distance, calories, time
                bikeTwoCurrUser.monthCaloriesDatabase = bikeTwoCurrUser.monthCaloriesDatabase + bike2Data.calories;
                bikeTwoCurrUser.monthDistanceDatabase = bikeTwoCurrUser.monthDistanceDatabase + bike2Data.distance;
                bikeTwoCurrUser.monthTimeDatabase = bikeTwoCurrUser.monthTimeDatabase + bikeTwoTimeDiff;

                //Place time, distance, calories into the "LastSessionStats columns"
                var userLastSessionCalories2 = bike2Data.calories;
                var userLastSessionDistance2 = bike2Data.distance;

                //Update the database
                con.query('UPDATE stats SET TotalCalories =' + bikeTwoCurrUser.totalCaloriesDatabase
                    + ', TotalDistance =' + bikeTwoCurrUser.totalDistanceDatabase
                    + ', TotalTime =' + bikeTwoCurrUser.totalTimeDatabase
                    + ', LastSessionCalories =' + userLastSessionCalories2
                    + ', LastSessionDistance =' + userLastSessionDistance2
                    + ', LastSessionTime =' + bikeTwoTimeDiff
                    + ', MonthCalories =' + bikeTwoCurrUser.monthCaloriesDatabase
                    + ', MonthDistance =' + bikeTwoCurrUser.monthDistanceDatabase
                    + ', MonthTime =' + bikeTwoCurrUser.monthTimeDatabase
                    + ' WHERE StudentID =' + bikeTwoCurrUser.studentIdentifier, function (err, rows) {
                    console.log(err);
                });

                updateDurationOfUse(bikeTwoTimeDiff, 2);

                //Set the student identifier back to default value to unlock loggin for other users
                bikeTwoCurrUser.studentIdentifier = USERLOGINLOCK;
                bikeTwoCurrUser.totalCaloriesDatabase = 0;
                bikeTwoCurrUser.totalDistanceDatabase = 0;
                bikeTwoCurrUser.totalTimeDatabase = 0;
                bikeTwoCurrUser.monthCaloriesDatabase = 0;
                bikeTwoCurrUser.monthDistanceDatabase = 0;
                bikeTwoCurrUser.monthTimeDatabase = 0;

                //Acknowledge the user session has ended
                io.emit('doneBikeTwoSession');

                // Set the timeout to 0 again
                bikeTwoTimeout = 0;
                console.log("Bike two timeout duration reached" + timeDiffTimeout2);
            }
        }
    }, (1000));

    socket.on('disconnect', function () {
        console.log("Socket disconnected");
    });

    //Socket for logging into bike 1
    socket.on('bikeOneStudentIdentifier', function (data) {
        var studentOneObject;

        console.log("A user with cwl: " + data + " is attempting to login to bike 1!");

        var hashTest = hashCode(data);

        //Check if student identifier is equal to default value (ie. nobody is logged in)
        if (bikeOneCurrUser.studentIdentifier == USERLOGINLOCK || hashCode(data) == bikeOneCurrUser.studentIdentifier) {
            console.log("Nobody is currently logged into bike 1. User with cwl = " + data + " will now log into bike 1.");

            //Validate the user who is attempting to log in
            findUserInDatabase(data, function (err, userObject) {
                if (err) {
                    console.log(err);
                }
                if (userObject[0] == undefined) {
                    //User doesn't exist in database, therefore, create it.
                    createUser(data, function (err, resultCallBack) {

                        //Check that the user was correctly added to the database, and retrieve them from it.
                        findUserInDatabase(data, function (err, userObjectNewlyMade) {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                studentOneObject = userObjectNewlyMade;

                                //Populate the global object with the user data
                                bikeOneCurrUser.studentIdentifier = hashCode(data); //StudentID

                                //Total Stats
                                bikeOneCurrUser.totalCaloriesDatabase = studentOneObject[0].TotalCalories;
                                bikeOneCurrUser.totalDistanceDatabase = studentOneObject[0].TotalDistance;
                                bikeOneCurrUser.totalTimeDatabase = studentOneObject[0].TotalTime;

                                //Monthly Stats
                                bikeOneCurrUser.monthCaloriesDatabase = studentOneObject[0].MonthCalories;
                                bikeOneCurrUser.monthDistanceDatabase = studentOneObject[0].MonthDistance;
                                bikeOneCurrUser.monthTimeDatabase = studentOneObject[0].MonthTime;
                            }
                        });
                    });
                } else {
                    studentOneObject = userObject;
                    console.log("A returned user is logging in with StudentID: " + studentOneObject[0].StudentID);

                    //Populate the global object with the user data

                    bikeOneCurrUser.studentIdentifier = hashCode(data);  //StudentID

                    //Total Stats
                    bikeOneCurrUser.totalCaloriesDatabase = studentOneObject[0].TotalCalories;
                    bikeOneCurrUser.totalDistanceDatabase = studentOneObject[0].TotalDistance;
                    bikeOneCurrUser.totalTimeDatabase = studentOneObject[0].TotalTime;

                    //Monthly Stats
                    bikeOneCurrUser.monthCaloriesDatabase = studentOneObject[0].MonthCalories;
                    bikeOneCurrUser.monthDistanceDatabase = studentOneObject[0].MonthDistance;
                    bikeOneCurrUser.monthTimeDatabase = studentOneObject[0].MonthTime;
                }
            });

            //No need to retrieve last session stats, as that will be the current session stats
            //Acknowledge that the user has logged in and to redirect to the dashboard page

            // Initialize bike 1 object for new user
            bike1Data.sessionStart = Date.now();
            bike1Data.rpm = 0;
            bike1Data.speed = 0;
            bike1Data.distance = 0;
            bike1Data.calories = 0;

            io.emit('doneBikeOneLogin');
        }
        else {
            console.log("Another user is currently logged in. In order to log in, stop pedalling for 3 minutes.");
            io.emit('userLoginBlocked1');
        }

        //Get BikeOne Stats from Database to use later
        findUserInDatabase("bikeone", function (err, bikeObjectFromDatabase) {
            if (err) {
                console.log(err);
            }
            else {
                bike1Data.databaseTotalCalories = bikeObjectFromDatabase[0].TotalCalories;
                bike1Data.databaseTotalDistance = bikeObjectFromDatabase[0].TotalDistance;
                bike1Data.databaseTotalTime = bikeObjectFromDatabase[0].TotalTime;

                bike1Data.databaseMonthCalories = bikeObjectFromDatabase[0].MonthCalories;
                bike1Data.databaseMonthDistance = bikeObjectFromDatabase[0].MonthDistance;
                bike1Data.databaseMonthTime = bikeObjectFromDatabase[0].MonthTime;
            }
        });


        //Get the use times from the table "saved_data" and store in the bike1 global object
        var currentDate = new Date(Date.now());
        var currentYear = currentDate.getFullYear();

        con.query("SELECT useTime_u30, useTime30_60, useTime60_120, useTime_o120 FROM saved_data WHERE Bike = 'BikeOne' AND Year =" + currentYear, function (err, rows) {
            if (err) {
                console.log(err);
            } else {
                bike1Data.useTime_u30 = rows[0].useTime_u30;
                bike1Data.useTime30_60 = rows[0].useTime30_60;
                bike1Data.useTime60_120 = rows[0].useTime60_120;
                bike1Data.useTime_o120 = rows[0].useTime_o120;
            }
        });


    });

    //socket for attempting to login to bike 2
    socket.on('bikeTwoStudentIdentifier', function (data) {
        var studentTwoObject;
        var hashTest = hashCode(data);

        //Check if student identifier is equal to default value (ie. nobody is logged in)
        if (bikeTwoCurrUser.studentIdentifier == USERLOGINLOCK || hashCode(data) == bikeTwoCurrUser.studentIdentifier) {

            //Validate the user who is attempting to log in
            findUserInDatabase(data, function (err, userObject) {
                if (err) {
                    console.log("ERROR: " + err);
                }
                if (userObject[0] == undefined) {
                    //User doesn't exist in database, therefore, create it.
                    createUser(data, function (err, resultCallBack) {
                        findUserInDatabase(data, function (err, userObjectNewlyMade) {
                            if (err) {
                                console.log("ERROR: " + err);
                            }
                            else {
                                studentTwoObject = userObjectNewlyMade;

                                //Populate the global object with the user data
                                bikeTwoCurrUser.studentIdentifier = hashCode(data); //StudentID

                                //Total Stats
                                bikeTwoCurrUser.totalCaloriesDatabase = studentTwoObject[0].TotalCalories;
                                bikeTwoCurrUser.totalDistanceDatabase = studentTwoObject[0].TotalDistance;
                                bikeTwoCurrUser.totalTimeDatabase = studentTwoObject[0].TotalTime;

                                //Monthly Stats
                                bikeTwoCurrUser.monthCaloriesDatabase = studentTwoObject[0].MonthCalories;
                                bikeTwoCurrUser.monthDistanceDatabase = studentTwoObject[0].MonthDistance;
                                bikeTwoCurrUser.monthTimeDatabase = studentTwoObject[0].MonthTime;
                            }
                        });
                    });
                } else {
                    studentTwoObject = userObject;
                    console.log("A returned user is logging in with StudentID: " + studentTwoObject[0].StudentID);

                    //Populate the global object with the user data

                    bikeTwoCurrUser.studentIdentifier = hashCode(data);  //StudentID

                    //Total Stats
                    bikeTwoCurrUser.totalCaloriesDatabase = studentTwoObject[0].TotalCalories;
                    bikeTwoCurrUser.totalDistanceDatabase = studentTwoObject[0].TotalDistance;
                    bikeTwoCurrUser.totalTimeDatabase = studentTwoObject[0].TotalTime;

                    //Monthly Stats
                    bikeTwoCurrUser.monthCaloriesDatabase = studentTwoObject[0].MonthCalories;
                    bikeTwoCurrUser.monthDistanceDatabase = studentTwoObject[0].MonthDistance;
                    bikeTwoCurrUser.monthTimeDatabase = studentTwoObject[0].MonthTime;
                }
            });

            //No need to retrieve last session stats, as that will be the current session stats
            //Acknowledge that the user has logged in and to redirect to the dashboard page

            // Initialize bike 2 data for new user
            bike2Data.sessionStart = Date.now();
            bike2Data.rpm = 0;
            bike2Data.speed = 0;
            bike2Data.distance = 0;
            bike2Data.calories = 0;

            io.emit('doneBikeTwoLogin');
        }
        else {
            console.log("Another user is currently logged in. In order to log in, stop pedalling for 3 minutes.");
            io.emit('userLoginBlocked2');
        }

        findUserInDatabase("biketwo", function (err, bikeObjectFromDatabase2) {
            if (err) {
                console.log(err);
            }
            else {
                bike2Data.databaseTotalCalories = bikeObjectFromDatabase2[0].TotalCalories;
                bike2Data.databaseTotalDistance = bikeObjectFromDatabase2[0].TotalDistance;
                bike2Data.databaseTotalTime = bikeObjectFromDatabase2[0].TotalTime;

                bike2Data.databaseMonthCalories = bikeObjectFromDatabase2[0].MonthCalories;
                bike2Data.databaseMonthDistance = bikeObjectFromDatabase2[0].MonthDistance;
                bike2Data.databaseMonthTime = bikeObjectFromDatabase2[0].MonthTime;
            }
        });


        //Get the use times from the table "saved_data" and store in the bike1 global object
        var currentDate = new Date(Date.now());
        var currentYear = currentDate.getFullYear();

        con.query("SELECT useTime_u30, useTime30_60, useTime60_120, useTime_o120 FROM saved_data WHERE Bike = 'BikeTwo' AND Year =" + currentYear, function (err, rows) {
            if (err) {
                console.log(err);
            } else {
                bike2Data.useTime_u30 = rows[0].useTime_u30;
                bike2Data.useTime30_60 = rows[0].useTime30_60;
                bike2Data.useTime60_120 = rows[0].useTime60_120;
                bike2Data.useTime_o120 = rows[0].useTime_o120;
            }
        });


    });

    socket.on('bikeOneEndSession', function () {
        console.log("User is ending their session!");

        //End session button has been pressed, place the stats into variables
        var bikeOneTimeDiff = Date.now() - bike1Data.sessionStart;
        bikeOneTimeDiff = Math.round(bikeOneTimeDiff / 60000);

        var currentDate = new Date(Date.now());
        var currentYear = currentDate.getFullYear();

        updateDurationOfUse(bikeOneTimeDiff, 1);

        //Update total distance, calories, time
        bikeOneCurrUser.totalCaloriesDatabase = bikeOneCurrUser.totalCaloriesDatabase + bike1Data.calories;
        bikeOneCurrUser.totalDistanceDatabase = bikeOneCurrUser.totalDistanceDatabase + bike1Data.distance;
        bikeOneCurrUser.totalTimeDatabase = bikeOneCurrUser.totalTimeDatabase + bikeOneTimeDiff;

        //Update monthly distance, calories, time
        bikeOneCurrUser.monthCaloriesDatabase = bikeOneCurrUser.monthCaloriesDatabase + bike1Data.calories;
        bikeOneCurrUser.monthDistanceDatabase = bikeOneCurrUser.monthDistanceDatabase + bike1Data.distance;
        bikeOneCurrUser.monthTimeDatabase = bikeOneCurrUser.monthTimeDatabase + bikeOneTimeDiff;

        //Place time, distance, calories into the "LastSessionStats columns"
        var userLastSessionCalories = bike1Data.calories;
        var userLastSessionDistance = bike1Data.distance;

        //Update the database for the user
        con.query('UPDATE stats SET TotalCalories =' + bikeOneCurrUser.totalCaloriesDatabase
            + ', TotalDistance =' + bikeOneCurrUser.totalDistanceDatabase
            + ', TotalTime =' + bikeOneCurrUser.totalTimeDatabase
            + ', LastSessionCalories =' + userLastSessionCalories
            + ', LastSessionDistance =' + userLastSessionDistance
            + ', LastSessionTime =' + bikeOneTimeDiff
            + ', MonthCalories =' + bikeOneCurrUser.monthCaloriesDatabase
            + ', MonthDistance =' + bikeOneCurrUser.monthDistanceDatabase
            + ', MonthTime =' + bikeOneCurrUser.monthTimeDatabase
            + ' WHERE StudentID =' + bikeOneCurrUser.studentIdentifier, function (err, rows) {
            console.log(err);
        });

        //Update the database for the bikeone total
        bike1Data.databaseTotalCalories = bike1Data.databaseTotalCalories + bike1Data.calories;
        bike1Data.databaseTotalDistance = bike1Data.databaseTotalDistance + bike1Data.distance;
        bike1Data.databaseTotalTime = bike1Data.databaseTotalTime + bikeOneTimeDiff;

        bike1Data.databaseMonthCalories = bike1Data.databaseMonthCalories + bike1Data.calories;
        bike1Data.databaseMonthDistance = bike1Data.databaseMonthDistance + bike1Data.distance;
        bike1Data.databaseMonthTime = bike1Data.databaseMonthTime + bikeOneTimeDiff;

        con.query('UPDATE stats SET TotalCalories =' + bike1Data.databaseTotalCalories
            + ', TotalDistance =' + bike1Data.databaseTotalDistance
            + ', TotalTime =' + bike1Data.databaseTotalTime
            + ', MonthCalories =' + bike1Data.databaseMonthCalories
            + ', MonthDistance =' + bike1Data.databaseMonthDistance
            + ', MonthTime =' + bike1Data.databaseMonthTime
            + ' WHERE StudentID = -110955803', function (err, rows) {
            console.log(err);
        });

        //Set the student identifier back to default value to unlock loggin for other users
        bikeOneCurrUser.studentIdentifier = USERLOGINLOCK;
        bikeOneCurrUser.totalCaloriesDatabase = 0;
        bikeOneCurrUser.totalDistanceDatabase = 0;
        bikeOneCurrUser.totalTimeDatabase = 0;
        bikeOneCurrUser.monthCaloriesDatabase = 0;
        bikeOneCurrUser.monthDistanceDatabase = 0;
        bikeOneCurrUser.monthTimeDatabase = 0;

        // Set Bike 1 data back to 0
        bike1Data.distance = 0;
        bike1Data.calories = 0;
        bikeOnePrevDistance = 0;

        // Query the database for the Bike daily distance values
        con.query('SELECT TotalDistance, MonthDistance FROM stats WHERE StudentID =' + bike1ID, function (err, rows) {
            if (rows[0] == null) {
                console.log(rows);
                console.log("Bike One TotalDistance query failed");
            } else {
                console.log(rows);
                bikeOneDailyDistance = Number(rows[0].TotalDistance);
                bikeOneMonthlyDistance = Number(rows[0].MonthDistance);
                console.log("Bike One Daily and Monthly Distance init: " + bikeOneDailyDistance + ", " + bikeOneMonthlyDistance);
            }
        });

        //Acknowledge the user session has ended
        io.emit('doneBikeOneSession');
    });


    socket.on('bikeTwoEndSession', function () {
        console.log("User is ending their session!");

        //End session button has been pressed, place the stats into variables
        var bikeTwoTimeDiff = Date.now() - bike2Data.sessionStart;
        bikeTwoTimeDiff = Math.round(bikeTwoTimeDiff / 60000);

        var currentDate = new Date(Date.now());
        var currentYear = currentDate.getFullYear();

        updateDurationOfUse(bikeTwoTimeDiff, 2);

        //Update total distance, calories, time
        bikeTwoCurrUser.totalCaloriesDatabase = bikeTwoCurrUser.totalCaloriesDatabase + bike2Data.calories;
        bikeTwoCurrUser.totalDistanceDatabase = bikeTwoCurrUser.totalDistanceDatabase + bike2Data.distance;
        bikeTwoCurrUser.totalTimeDatabase = bikeTwoCurrUser.totalTimeDatabase + bikeTwoTimeDiff;

        //Update monthly distance, calories, time
        bikeTwoCurrUser.monthCaloriesDatabase = bikeTwoCurrUser.monthCaloriesDatabase + bike2Data.calories;
        bikeTwoCurrUser.monthDistanceDatabase = bikeTwoCurrUser.monthDistanceDatabase + bike2Data.distance;
        bikeTwoCurrUser.monthTimeDatabase = bikeTwoCurrUser.monthTimeDatabase + bikeTwoTimeDiff;

        //Place time, distance, calories into the "LastSessionStats columns"
        var userLastSessionCalories = bike2Data.calories;
        var userLastSessionDistance = bike2Data.distance;

        //Update the database
        con.query('UPDATE stats SET TotalCalories =' + bikeTwoCurrUser.totalCaloriesDatabase
            + ', TotalDistance =' + bikeTwoCurrUser.totalDistanceDatabase
            + ', TotalTime =' + bikeTwoCurrUser.totalTimeDatabase
            + ', LastSessionCalories =' + userLastSessionCalories
            + ', LastSessionDistance =' + userLastSessionDistance
            + ', LastSessionTime =' + bikeTwoTimeDiff
            + ', MonthCalories =' + bikeTwoCurrUser.monthCaloriesDatabase
            + ', MonthDistance =' + bikeTwoCurrUser.monthDistanceDatabase
            + ', MonthTime =' + bikeTwoCurrUser.monthTimeDatabase
            + ' WHERE StudentID =' + bikeTwoCurrUser.studentIdentifier, function (err, rows) {
            console.log(err);
        });


        bike2Data.databaseTotalCalories = bike2Data.databaseTotalCalories + bike2Data.calories;
        bike2Data.databaseTotalDistance = bike2Data.databaseTotalDistance + bike2Data.distance;
        bike2Data.databaseTotalTime = bike2Data.databaseTotalTime + bikeTwoTimeDiff;

        bike2Data.databaseMonthCalories = bike2Data.databaseMonthCalories + bike2Data.calories;
        bike2Data.databaseMonthDistance = bike2Data.databaseMonthDistance + bike2Data.distance;
        bike2Data.databaseMonthTime = bike2Data.databaseMonthTime + bikeTwoTimeDiff;

        con.query('UPDATE stats SET TotalCalories =' + bike2Data.databaseTotalCalories
            + ', TotalDistance =' + bike2Data.databaseTotalDistance
            + ', TotalTime =' + bike2Data.databaseTotalTime
            + ', MonthCalories =' + bike2Data.databaseMonthCalories
            + ', MonthDistance =' + bike2Data.databaseMonthDistance
            + ', MonthTime =' + bike2Data.databaseMonthTime
            + ' WHERE StudentID = -110950709', function (err, rows) {
            console.log(err);
        });


        //Set the student identifier back to default value to unlock loggin for other users
        bikeTwoCurrUser.studentIdentifier = USERLOGINLOCK;
        bikeTwoCurrUser.totalCaloriesDatabase = 0;
        bikeTwoCurrUser.totalDistanceDatabase = 0;
        bikeTwoCurrUser.totalTimeDatabase = 0;
        bikeTwoCurrUser.monthCaloriesDatabase = 0;
        bikeTwoCurrUser.monthDistanceDatabase = 0;
        bikeTwoCurrUser.monthTimeDatabase = 0;

        // Set Bike 1 data back to 0
        bike2Data.distance = 0;
        bike2Data.calories = 0;
        bikeTwoPrevDistance = 0;

        // Query the database for the Bike daily distance values
        con.query('SELECT TotalDistance, MonthDistance FROM stats WHERE StudentID =' + bike2ID, function (err, rows) {
            if (rows[0] == null) {
                console.log(rows);
                console.log("Bike Two TotalDistance query failed");
            } else {
                bikeTwoDailyDistance = Number(rows[0].TotalDistance);
                bikeTwoMonthlyDistance = Number(rows[0].MonthDistance);
                console.log("Bike Two Daily and Monthly Distance init: " + bikeTwoDailyDistance + ", " + bikeTwoMonthlyDistance);
            }
        });

        //Acknowledge the user session has ended
        io.emit('doneBikeTwoSession');
    });

    //Socket for Viewing Statistics in modal login page
    socket.on('viewStatisticsSocket', function (data) {
        console.log("User is attempting to view their statistics...");
        getUserStats(hashCode(data), socket);
    });

    socket.on('bike1Cadence', function (data) {
        bike1Data.rpm = data;

        var bikeCurTime1 = Date.now();

        // Calorie Calculation
        // MET value from Compendium of Physical Activities: 3.5
        // Average body weight: 70kg
        // MET X weight in KG X duration in hours
        bike1Data.speed = bikeGearRatio * bike1Data.rpm * bikeCircumference * (60 / 1000);

        //console.log("Bike 1 speed vars: " + bike1Data.rpm + " " + bike1Data.speed + " " + bikeCurTime1 + " " + bikePrevTime1 + " " + bike1Data.distance);
        timeDiff1 = (bikeCurTime1 - bikePrevTime1);

        // Fix for time differential error caused by having no readings for a long amount of time
        if (timeDiff1 > 3500) {
            timeDiff1 = 2500;
        }
        timeDiff1 = timeDiff1 / (1000 * 3600);

        bikePrevTime1 = bikeCurTime1;

        bike1Data.distance = bike1Data.distance + (bike1Data.speed * timeDiff1) * 1000;

        //console.log("Bike 1 speed vars: " + bike1Data.rpm + " " + bike1Data.speed + " " + timeDiff1 + " " + bike1Data.distance);
        bike1Data.calories = bike1Data.calories + (MET * avgWeight * timeDiff1);

        if (bike1Data.rpm != 0) {
            bikeOneTimeout = 0;
            console.log("Bike One Timeout Reset");
        }

        // console.log(bike1Data);

        // Overall usage tracking
        // For anytime there is no user logged in and RPM is not 0, accumulate a daily counter
        if ((bikeOneCurrUser.studentIdentifier == USERLOGINLOCK) && (bike1Data.rpm != 0)) {
            // accumulate a daily counter of the distance
            bikeOneDailyDistance = bikeOneDailyDistance + (bike1Data.distance - bikeOnePrevDistance);
            bikeOneMonthlyDistance = bikeOneMonthlyDistance + (bike1Data.distance - bikeOnePrevDistance);

            bikeOnePrevDistance = bike1Data.distance;
            bikeOneDDUpdateFlag = 1;
            console.log("Bike One accumulated daily and monthly distance: " + bikeOneDailyDistance + ", " + bikeOneMonthlyDistance);
        }

        // When Bike1 RPM == 0 and the distance changed, write the accumulated value to the database
        if ((bike1Data.rpm == 0) && (bikeOneDDUpdateFlag == 1)) {
            con.query('UPDATE stats SET TotalDistance =' + bikeOneDailyDistance
                + ', MonthDistance =' + bikeOneMonthlyDistance
                + ' WHERE StudentID = -110955803', function (err, rows) {
                console.log(err);
            });

            bikeOneDDUpdateFlag = 0;

            // Query the database for the Bike daily distance values
            con.query('SELECT TotalDistance, MonthDistance FROM stats WHERE StudentID =' + bike1ID, function (err, rows) {
                if (rows[0] == null) {
                    console.log(rows);
                    console.log("Bike One TotalDistance query failed");
                } else {
                    console.log(rows);
                    bikeOneDailyDistance = Number(rows[0].TotalDistance);
                    bikeOneMonthlyDistance = Number(rows[0].MonthDistance);
                    console.log("Bike One Daily and Monthly Distance init: " + bikeOneDailyDistance + ", " + bikeOneMonthlyDistance);
                }
            });
            console.log("Update Bike One TotalDistance, MonthDistance with: " + bikeOneDailyDistance + ", " + bikeOneMonthlyDistance);
        }

        //send data to testApp.js
        io.emit('bike1rpm', bike1Data.rpm);
        io.emit('bike1speed', bike1Data.speed);
        io.emit('bike1distance', bike1Data.distance);
        io.emit('bike1calories', bike1Data.calories);
    });

    socket.on('bike2Cadence', function (data) {
        bike2Data.rpm = data;

        var bikeCurTime2 = Date.now();

        // Calorie Calculation
        // MET value from Compendium of Physical Activities: 3.5
        // Average body weight: 70kg
        // MET X weight in KG X duration in hours
        bike2Data.speed = bikeGearRatio * bike2Data.rpm * bikeCircumference * (60 / 1000);

        timeDiff2 = (bikeCurTime2 - bikePrevTime2);

        // Fix for time differential error caused by having no readings for a long amount of time
        if (timeDiff2 > 3500) {
            timeDiff2 = 2500;
        }
        timeDiff2 = timeDiff2 / (1000 * 3600);
        bikePrevTime2 = bikeCurTime2;

        bike2Data.distance = bike2Data.distance + (bike2Data.speed * timeDiff2) * 1000;

        bike2Data.calories = bike2Data.calories + (MET * avgWeight * timeDiff2);

        if (bike2Data.rpm != 0) {
            bikeTwoTimeout = 0;
            console.log("Bike Two Timeout Reset");
        }

        // Overall usage tracking
        // Accumulate the distance while RPM is > 0
        if ((bikeTwoCurrUser.studentIdentifier == USERLOGINLOCK) && (bike2Data.rpm != 0)) {
            // accumulate a daily counter of the distance
            bikeTwoDailyDistance = bikeTwoDailyDistance + (bike2Data.distance - bikeTwoPrevDistance);
            bikeTwoMonthlyDistance = bikeTwoMonthlyDistance + (bike2Data.distance - bikeTwoPrevDistance);

            bikeTwoPrevDistance = bike2Data.distance;
            bikeTwoDDUpdateFlag = 1;
            console.log("Bike Two accumulated daily and monthly distance: " + bikeTwoDailyDistance + ", " + bikeTwoMonthlyDistance);
        }

        // When Bike2 RPM == 0 and the distance changed, write the accumulated value to the database
        if ((bike2Data.rpm == 0) && (bikeTwoDDUpdateFlag == 1)) {
            con.query('UPDATE stats SET TotalDistance =' + bikeTwoDailyDistance
                + ', MonthDistance =' + bikeTwoMonthlyDistance
                + ' WHERE StudentID = -110950709', function (err, rows) {
                console.log(err);
            });

            bikeTwoDDUpdateFlag = 0;

            con.query('SELECT TotalDistance, MonthDistance FROM stats WHERE StudentID =' + bike2ID, function (err, rows) {
                if (rows[0] == null) {
                    console.log(rows);
                    console.log("Bike Two TotalDistance query failed");
                } else {
                    bikeTwoDailyDistance = Number(rows[0].TotalDistance);
                    bikeTwoMonthlyDistance = Number(rows[0].MonthDistance);
                    console.log("Bike Two Daily and Monthly Distance init: " + bikeTwoDailyDistance + ", " + bikeTwoMonthlyDistance);
                }
            });
            console.log("Update Bike Two TotalDistance, MonthDistance with: " + bikeTwoDailyDistance + ", " + bikeTwoMonthlyDistance);
        }

        //send data to testApp.js
        io.emit('bike2rpm', bike2Data.rpm);
        io.emit('bike2speed', bike2Data.speed);
        io.emit('bike2distance', bike2Data.distance);
        io.emit('bike2calories', bike2Data.calories);
    });


    // GRAPH EVENTS
    socket.on("dateCheck", function () {
        if (startFlag == 0) {
            // server.js has just started or been restarted
            // get the date information
            date = new Date(Date.now());
            month = date.getMonth();
            year = date.getFullYear();
            startFlag = 1;
        }

        // this will execute everytime the stats page has been loaded/visited
        var curDate = new Date(Date.now());
        // want these to be able to compare to when this file was first started
        var curMonth = curDate.getMonth();
        var curYear = curDate.getFullYear();

        if (curYear != year) {
            // create two new entries, one for each with year being the curYear
            year = curYear;
            // create the entries here  
            con.query("INSERT INTO saved_data (Bike, January, February, March, April, May, June, July, August, September, October, November, December, Year,"
                + " useTime_u30, useTime30_60, useTime60_120, useTime_o120) "
                + " VALUES ('BikeOne', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " + curYear + ", 0, 0, 0, 0)", function (err, result) {
                if (err) {
                    console.log("INSERT ERROR: " + err);
                } else {
                    console.log("New record added successfully");
                }
            });

            con.query("INSERT INTO saved_data (Bike, January, February, March, April, May, June, July, August, September, October, November, December, Year,"
                + " useTime_u30, useTime30_60, useTime60_120, useTime_o120) "
                + " VALUES ('BikeTwo', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " + curYear + ", 0, 0, 0, 0)", function (err, result) {
                if (err) {
                    console.log("INSERT ERROR: " + err);
                } else {
                    console.log("New record added successfully");
                }
            });

        }

        if (curMonth != month) {
            // if we are in a new month then need to write to the database and update client side
            // set the month to be the new month
            month = curMonth;
            updateGraphData(socket, month, curYear);
        } else {
            io.emit("dateCheckFinished");
        }
    });

    socket.on("getMonthlyStats", function () {
        var monthResults = {
            bike1: [],
            bike2: []
        };

        var totalResults = {
            bike1: [],
            bike2: [],
            total: []
        };

        var date = new Date(Date.now());
        var curMonth = date.getMonth();
        var curYear = date.getFullYear();

        // get all months from the saved_data table for each bike
        con.query("SELECT January, February, March, April, " +
            "May, June, July, August, September, October, November, " +
            "December FROM saved_data WHERE Bike = 'BikeOne' AND Year = " + curYear, function (err, rows) {
            if (rows[0] == null) {
                socket.emit("bikeResults", -1);
                return;
            } else {
                // just want the numeric data, not the months as well
                monthResults.bike1.push(rows[0].January);
                monthResults.bike1.push(rows[0].February);
                monthResults.bike1.push(rows[0].March);
                monthResults.bike1.push(rows[0].April);
                monthResults.bike1.push(rows[0].May);
                monthResults.bike1.push(rows[0].June);
                monthResults.bike1.push(rows[0].July);
                monthResults.bike1.push(rows[0].August);
                monthResults.bike1.push(rows[0].September);
                monthResults.bike1.push(rows[0].October);
                monthResults.bike1.push(rows[0].November);
                monthResults.bike1.push(rows[0].December);
            }

            con.query("SELECT January, February, March, April, " +
                "May, June, July, August, September, October, November, " +
                "December FROM saved_data WHERE Bike = 'BikeTwo' AND Year = " + curYear, function (err, rows) {
                if (rows[0] == null) {
                    socket.emit("bikeResults", -1);
                    return;
                } else {
                    // just want the numeric data, not the months as well
                    monthResults.bike2.push(rows[0].January);
                    monthResults.bike2.push(rows[0].February);
                    monthResults.bike2.push(rows[0].March);
                    monthResults.bike2.push(rows[0].April);
                    monthResults.bike2.push(rows[0].May);
                    monthResults.bike2.push(rows[0].June);
                    monthResults.bike2.push(rows[0].July);
                    monthResults.bike2.push(rows[0].August);
                    monthResults.bike2.push(rows[0].September);
                    monthResults.bike2.push(rows[0].October);
                    monthResults.bike2.push(rows[0].January);
                    monthResults.bike2.push(rows[0].January);
                }

                // emit here so that both bike objects data have been included
                socket.emit("monthlyResults", monthResults);

                var index;
                var i;
                var totalDistance = 0;
                var bike1Total = 0;
                var bike2Total = 0;

                if (curMonth == 0) {
                    index = 11;
                    // will have a full years worth of data
                    for (i = 0; i <= index; i++) {
                        bike1Total = bike1Total + Number(monthResults.bike1[i]);
                        bike2Total = bike2Total + Number(monthResults.bike2[i]);
                        totalDistance = totalDistance + Number(monthResults.bike1[i]) + Number(monthResults.bike2[i]);

                        totalResults.bike1[i] = bike1Total.toFixed(3);
                        totalResults.bike2[i] = bike2Total.toFixed(3);
                        totalResults.total[i] = totalDistance.toFixed(3);
                    }
                } else {
                    // we are in a new month, so we have data for everything up till now
                    // e.g. it is now March, we have monthly totals for January and February
                    for (i = 0; i < curMonth; i++) {
                        bike1Total = bike1Total + Number(monthResults.bike1[i]);
                        bike2Total = bike2Total + Number(monthResults.bike2[i]);
                        totalDistance = totalDistance + Number(monthResults.bike1[i]) + Number(monthResults.bike2[i]);

                        totalResults.bike1[i] = bike1Total.toFixed(3);
                        totalResults.bike2[i] = bike2Total.toFixed(3);
                        totalResults.total[i] = totalDistance.toFixed(3);
                    }
                }
                socket.emit("totalResults", totalResults);
            });
        });


    });

    socket.on("getCurMonthStats", function () {
        var results = {
            bike1: [],
            bike2: []
        }

        var totalDist = 0;
        var bike1Dist = 0;
        var bike2Dist = 0;

        con.query("SELECT MonthDistance FROM stats WHERE StudentID = " + bike1ID, function (err, rows) {
            if (rows[0].MonthDistance == null) {
                socket.emit("currentMonthStats", -1);
                return;
            } else {
                console.log("current monthly distance bike1: " + rows[0].MonthDistance);
                bike1Dist = Number(rows[0].MonthDistance) / 1000;
                results.bike1.push(bike1Dist.toFixed(3));
            }

            con.query("SELECT MonthDistance FROM stats WHERE StudentID = " + bike2ID, function (err, rows) {
                if (rows[0].MonthDistance == null) {
                    socket.emit("currentMonthStats", -1);
                    return;
                } else {
                    console.log("current monthly distance bike2: " + rows[0].MonthDistance);
                    bike2Dist = Number(rows[0].MonthDistance) / 1000;
                    results.bike2.push(bike2Dist.toFixed(3));
                }
                socket.emit("currentMonthStats", results);
            });
        });
    });


    socket.on("getUseTimeStats", function () {
        console.log("inside getUseTimeStats");
        var date = new Date(Date.now());
        var curYear = date.getFullYear();

        var results = [];
        var bike1 = [];
        var bike2 = [];

        con.query("SELECT useTime_u30, useTime30_60, useTime60_120, useTime_o120 "
            + "FROM saved_data WHERE Bike = 'BikeOne' AND Year = " + curYear, function (err, rows) {
            bike1.push(rows[0].useTime_u30);
            bike1.push(rows[0].useTime30_60);
            bike1.push(rows[0].useTime60_120);
            bike1.push(rows[0].useTime_o120);

            con.query("SELECT useTime_u30, useTime30_60, useTime60_120, useTime_o120 "
                + "FROM saved_data WHERE Bike = 'BikeTwo' AND Year = " + curYear, function (err, rows) {
                bike2.push(rows[0].useTime_u30);
                bike2.push(rows[0].useTime30_60);
                bike2.push(rows[0].useTime60_120);
                bike2.push(rows[0].useTime_o120);

                for (var i = 0; i < bike1.length; i++) {
                    results[i] = bike1[i] + bike2[i];
                }

                console.log("emitting use time results");
                socket.emit("useTimeResults", results);
            });
        });
    });
});


/** Function to add a new user to the database. It is assumed that this user does not already exist, as a prior check will be conducted
 * Parameters: studentID - The student number of the user to add
 */
function createUser(cwl, createUserCallBack) {
    console.log("Attempting to create a new user...");

    var hashValue = hashCode(cwl);

    con.query("INSERT INTO stats (CwlUsername, StudentID, TotalCalories, TotalDistance, TotalTime, LastSessionCalories, LastSessionDistance, LastSessionTime, MonthCalories, MonthDistance, MonthTime) VALUES (" + '"' + cwl + '"' + "," + hashValue + ", 0, 0, 0, 0, 0, 0, 0, 0, 0)", function (err, result) {
        if (err) {
            console.log(err);
            createUserCallBack(err, null);
        } else {
            createUserCallBack(null, result);
        }

        console.log("A new user has successfully been created!");
    });
}


/** A function to query the database to find a specified user based on the indentifier. This function will be used when attempting to login
 * Parameter: cwlUsername = the identifier for a user
 * Return: rows = The row of data for the specified user (The user was successfully found in the database)
 */
function findUserInDatabase(cwlUsername, findUserCallBack) {
    var hashValue = hashCode(cwlUsername);

    con.query('SELECT * FROM stats WHERE StudentID =' + hashValue, function (err, rows) {
        if (err) {
            findUserCallBack(err, null);
        }
        else {
            findUserCallBack(null, rows);
        }

    });
}

/** A function to retrieve a row from the database belonging to a specified user. This function is used to populate a modal on the login page with user data.
 * Parameter: studentID = the identifier for a user
 * Parameter: socket = the socket to emit the row of data to. The data will be collected within testApp.js within the viewStatistics function.
 */
function getUserStats(studentID, socket) {
    con.query('SELECT * FROM stats WHERE StudentID =' + studentID, function (err, rows) {
        if (rows[0] == null) {
            console.log(rows);
            console.log("That user was not in the database! Please enter a new user.");
            socket.emit('studentStats', -1);
        } else {
            socket.emit('studentStats', rows);
        }
    });
}


function hashCode(string) {
    var hash = 0, length = string.length, i = 0;
    if (length > 0)
        while (i < length)
            hash = (hash << 5) - hash + string.charCodeAt(i++) | 0;
    return hash;
};


function updateGraphData(socket, curMonth, curYear) {
    console.log("UPDATING GRAPH DATA");
    console.log("graph data month: " + curMonth);
    // array for the months so that we can convert integer to string
    var MONTHS = ["January", "February", "March", "April", "May", "June", "July",
        "August", "September", "October", "November", "December"];

    var updates = [];

    // nest these database calls so that they happen sequentially
    con.query("SELECT MonthDistance FROM stats WHERE StudentID = " + bike1ID, function (err, rows) {
        if (rows[0] == null) {
            console.log("No monthly data for bike 1");
            return;
        } else {
            console.log(rows[0]);
            updates.push(rows[0]);
        }

        con.query("SELECT MonthDistance FROM stats WHERE StudentID = " + bike2ID, function (err, rows) {
            if (rows[0] == null) {
                console.log("No monthly data for bike 2");
                return;
            } else {
                console.log(rows[0]);
                updates.push(rows[0]);
            }

            console.log("after query to stats table");
            // see if the data was returned
            console.log("data returned: " + updates);

            // we are here because the month has changed, but we want to write the data for the previous month
            if (curMonth == 0) {
                // January to December
                // set to 12 b/c we go do curMonth - 1 when updating below
                curMonth = 12;
            }

            // write monthly totals to new database, divide by 1000 to get kms
            // bike 1
            console.log("curMonth before saved_data update: " + curMonth);
            con.query("UPDATE saved_data SET " + MONTHS[curMonth - 1] + " = " + Math.round(updates[0].MonthDistance) / 1000
                + " WHERE Year = " + curYear
                + " AND Bike = 'BikeOne'", function (err, rows) {
                console.log(err);

                // bike 2
                con.query("UPDATE saved_data SET " + MONTHS[curMonth - 1] + " = " + Math.round(updates[1].MonthDistance) / 1000
                    + " WHERE Year = " + curYear
                    + " AND Bike = 'BikeTwo'", function (err, rows) {
                    console.log(err);

                    // have the data from the stats table now in updates... can clear the monthly data now
                    con.query("UPDATE stats SET MonthDistance = " + 0
                        + ", MonthCalories = " + 0 + ", MonthTime = " + 0, function (err, rows) {
                        console.log(err);
                        console.log("First update");

                        con.query("UPDATE stats SET MonthDistance = " + 0
                            + ", MonthCalories = " + 0 + ", MonthTime = " + 0, function (err, rows) {
                            console.log(err);
                            console.log("second update");
                        });
                    });
                });
            });
        });
    });

    // check to see if the month is September
    // if it is September, truncate the stats table so that it doesn't grow too large
    // then re enter the bikeone and biketwo entries

    if (curMonth == SEPTEMBER) {
        console.log("try truncating the stats table");
        truncateTable();
    }

    socket.emit("dateCheckFinished");
}

function truncateTable() {
    // it is september, so truncate the table and re enter the bike objects
    // nest the calls so that it executes sequentially
    con.query("TRUNCATE TABLE stats", function(err) {
        if (err) {
            console.log("Failed to truncate stats table");
            console.log(err);
        } else {
            // truncate worked but now we have no entries
            // add bikeone and biketwo 
            // bike1ID is the hash value for bikeone and vice versa for bike 2
            con.query("INSERT INTO stats (CwlUsername, StudentID, TotalCalories, TotalDistance, TotalTime, " 
                + "LastSessionCalories, LastSessionDistance, LastSessionTime, MonthCalories, MonthDistance, MonthTime) "
                + "VALUES (" + "'bikeone'," + bike1ID + ", 0, 0, 0, 0, 0, 0, 0, 0, 0)", function (err, result) {

                if (err) {
                    console.log(err);
                } else {
                    console.log("bikeone record added");
                }

                // now try adding biketwo
                con.query("INSERT INTO stats (CwlUsername, StudentID, TotalCalories, TotalDistance, TotalTime, " 
                    + "LastSessionCalories, LastSessionDistance, LastSessionTime, MonthCalories, MonthDistance, MonthTime) "
                    + "VALUES (" + "'biketwo'," + bike2ID + ", 0, 0, 0, 0, 0, 0, 0, 0, 0)", function (err, result) {

                    if (err) {
                        console.log(err);
                    } else {
                        console.log("biketwo record added");
                    }
                });
            });
        }
    });
}

function updateDurationOfUse(duration, bike) {
    // the use time data is updated when a user logs in
    // we don't currently have a way of tracking use time when someone is not logged in

    var date = new Date(Date.now());
    var currentYear = date.getFullYear();

    // if bike = 1, update bike 1
    if (bike == 1) {
        //Logic for Users Time spent on bike
        if (duration <= 30) {
            bike1Data.useTime_u30 += 1;
        } else if (duration > 30 && bikeOneTimeDiff <= 60) {
            bike1Data.useTime30_60 += 1;
        } else if (duration > 60 && bikeOneTimeDiff <= 120) {
            bike1Data.useTime60_120 += 1;
        } else {
            bike1Data.useTime_o120 += 1;
        }

        // Update the "saved_data" table with the use time variables
        con.query("UPDATE saved_data SET useTime_u30 =" + bike1Data.useTime_u30
            + ", useTime30_60 =" + bike1Data.useTime30_60
            + ", useTime60_120 =" + bike1Data.useTime60_120
            + ", useTime_o120 =" + bike1Data.useTime_o120
            + " WHERE Bike = 'BikeOne' AND Year = " + currentYear, function (err, rows) {
                console.log(err);
        });
    } else if (bike == 2) {
        //Logic for Users Time spent on bike
        if (duration <= 30) {
            bike2Data.useTime_u30 += 1;
        } else if (duration > 30 && bikeOneTimeDiff <= 60) {
            bike2Data.useTime30_60 += 1;
        } else if (duration > 60 && bikeOneTimeDiff <= 120) {
            bike2Data.useTime60_120 += 1;
        } else {
            bike2Data.useTime_o120 += 1;
        }

        // Update the "saved_data" table with the use time variables
        con.query("UPDATE saved_data SET useTime_u30 =" + bike2Data.useTime_u30
            + ", useTime30_60 =" + bike2Data.useTime30_60
            + ", useTime60_120 =" + bike2Data.useTime60_120
            + ", useTime_o120 =" + bike2Data.useTime_o120
            + " WHERE Bike = 'BikeTwo' AND Year = " + currentYear, function (err, rows) {
                console.log(err);
        });
    }
}



