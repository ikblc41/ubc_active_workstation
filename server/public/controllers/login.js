"use strict";

var app = angular.module("App.controllers", []);

app.controller("loginController", function($scope, $window) {
	$scope.master = {};
	$scope.cwl = {};

	var dataFlag = 0;

	$scope.bikeOneLogin = function(cwl) {
		var socket = io.connect();
		$scope.master = angular.copy(cwl);
		console.log("cwl.username: " + cwl.username);
		console.log("scope.cwl: " + $scope.cwl); 

		// sanitize the cwl input
        // must input something in order to use to workstation
        if(cwl.username == undefined || cwl.username == ""){
			window.alert("Please enter your CWL username!"); 
			return; 
		}
		// cwl username must be between 2 and 8 characters
		if(cwl.username.length < 2 || cwl.username.length > 8) {
			console.log(cwl.username.length);
			window.alert("The CWL username entered does not match the username requirements"
				+ "\n" + "\n" + "Please try again!");
			return;
		} 
		// the first two characters must be alphabetic
		else {
			for(var i = 0; i < 2; i++) {
				// NaN will return true for letters
				if(!isNaN(cwl.username[i])) {
					window.alert("Invalid CWL username. The first two characters must be letters.")
					return;
				}
			}
		}

		socket.emit('bikeOneStudentIdentifier', cwl.username);
		
		//Wait for acknowledgement that the user has logged in and to redirect to dashboard
		socket.on('doneBikeOneLogin', function() {
			// Direct to dashboard_bike1.html
        	// $window.location.href = 'dashboard_bike1.html';
        	// Direct to bike1_clf.html
        	$window.location.href = 'bike1.html';
		});

		socket.on('userLoginBlocked1', function(){
            window.alert("There is already a user logged into Bike 1!");
            return;
		});
  	}
  
  
	$scope.bikeTwoLogin = function(cwl) {
        var socket = io.connect();
        $scope.master = angular.copy(cwl);
        console.log("cwl.username: " + cwl.username);
        console.log("scope.cwl: " + $scope.cwl);

        // sanitize the cwl input
        // must input something in order to use to workstation
        if(cwl.username == undefined || cwl.username == ""){
			window.alert("Please enter your CWL username!"); 
			return; 
		}
		// cwl username must be between 2 and 8 characters
		if(cwl.username.length < 2 || cwl.username.length > 8) {
			console.log(cwl.username.length);
			window.alert("The CWL username entered does not match the username requirements"
				+ "\n" + "\n" + "Please try again!");
			return;
		} 
		// the first two characters must be alphabetic
		else {
			for(var i = 0; i < 2; i++) {
				// NaN will return true for letters
				if(!isNaN(cwl.username[i])) {
					window.alert("Invalid CWL username. The first two characters must be letters.")
					return;
				}
			}
		}

        socket.emit('bikeTwoStudentIdentifier', cwl.username);

        //Wait for acknowledgement that the user has logged in and to redirect to dashboard
        socket.on('doneBikeTwoLogin', function() {

            // Direct to dashboard_bike1.html
            // $window.location.href = 'dashboard_bike2.html';
            // Direct to bike2_clf.html
        	$window.location.href = 'bike2.html';
        });
        socket.on('userLoginBlocked2', function(){
            window.alert("There is already a user logged into Bike 2!");
            return;
        });
  	}
  
  	$scope.viewStatistics = function(cwl) {
  		var socket = io.connect();
  		$scope.master = angular.copy(cwl);
		console.log("cwl.username: " + cwl.username);

		if(cwl.username == undefined || cwl.username == "") {
			window.alert("Please enter your CWL username!");
			return;
		}
			  
		socket.emit('viewStatisticsSocket', cwl.username); 
		
		var button = document.getElementById("showModal");
		button.disabled = true;
		console.log("button disabled");

		socket.on("studentStats", function(data) {			
			if(data == -1) {
				//alert the user that there is no data for the specified user
				window.alert("Sorry, but we could not find that user in the database!");
				button.disabled = false;
				return;
			} else {
				//populate it with the user data
				modalSetup();  
				populateModal(data);
				button.disabled = false;
                		console.log("button enabled");
				return;
			}
		});
	}


	function modalSetup() {
  		var modal = document.getElementById("statsModal");
		var closeX = document.getElementById("statsClose");
		var table = document.getElementById("statsTable");

		modal.style.display = "block";

		closeX.onclick = function() {
			modal.style.display = "none";
			clearRows(dataFlag);
		}

		window.onclick = function(event) {
			if (event.target == modal) {
				modal.style.display = "none";
				clearRows(dataFlag);
			}
		}
	}

	function populateModal(rows) {
		
		console.log("populate modal with: " + rows[0]);
		var header = document.getElementById("id");
		header.textContent = "ID: " + rows[0].CwlUsername;

		var row1 = document.getElementById("statsTable").insertRow(-1);
		row1.setAttribute("id", "r_lastSession");
		row1.insertCell(0).textContent = "Last Session";
		row1.insertCell(1).textContent = rows[0].LastSessionCalories;
		row1.insertCell(2).textContent = rows[0].LastSessionDistance;
		row1.insertCell(3).textContent = rows[0].LastSessionTime;

		var row2 = document.getElementById("statsTable").insertRow(-1)
		row2.setAttribute("id", "r_monthly");
		row2.insertCell(0).textContent = "Monthly"
		row2.insertCell(1).textContent = rows[0].MonthCalories;
		row2.insertCell(2).textContent = rows[0].MonthDistance;
		row2.insertCell(3).textContent = rows[0].MonthTime;

		var row3 = document.getElementById("statsTable").insertRow(-1)
		row3.setAttribute("id", "r_total");
		row3.insertCell(0).textContent = "Total"
		row3.insertCell(1).textContent = rows[0].TotalCalories;
		row3.insertCell(2).textContent = rows[0].TotalDistance;
		row3.insertCell(3).textContent = rows[0].TotalTime;
	
		dataFlag = 1;
	}

	function clearRows(dataFlag) {
		var table = document.getElementById("statsTable");
		
		if(dataFlag) {		
			for (var i = 0; i < 3; i++) {
				table.deleteRow(-1);
			}
			var header = document.getElementById("id");
			header.textContent = "";
			dataFlag = 0;
		}
	}
});

function hashCode(string) {
    var hash = 0, length = string.length, i = 0;
    if ( length > 0 )
        while (i < length)
            hash = (hash << 5) - hash + string.charCodeAt(i++) | 0;
    return hash;
};
