"use strict";

var app = angular.module("App.controllers", []);

app.controller("bike1Controller", function ($scope, $interval, $window) {

    var socket = io.connect();
    var start = Date.now();
    var diff, min, sec = 0;

    // configure the RPM chart for the bike
    var config_bike1 = config_bike1Chart();

    // load the chart for the RPM
    $window.onload = function () {
        var ctx = document.getElementById('bike1RPMChart').getContext('2d');
        $window.bikeRPM = new Chart(ctx, config_bike1);
    };


    // listen for data with cadence tag, and assign it to
    socket.on("bike1rpm", function (rpm) {
        // console.log(data);
        $scope.$apply(function () {
            $scope.bike1RPMData = Math.round(rpm)
        });
        // add the new RPM data to the chart
        config_bike1.data.labels.push("     ");
        config_bike1.data.datasets[0].data.push(Math.round(rpm));
        $window.bikeRPM.update();

    });

    socket.on("bike1speed", function (data) {
        // console.log(data);
        $scope.$apply(function () {
            $scope.bike1SpeedData = Math.round(data)
        });

    });

    socket.on("bike1distance", function (data) {
        // console.log(data);
        $scope.$apply(function () {
            $scope.bike1DistData = Math.round(data)
        });
    });

    socket.on("bike1calories", function (data) {
        // console.log(data);
        $scope.$apply(function () {
            $scope.bike1CalData = Math.round(data)
        });
    });

    var timer = function () {
        var cur = Date.now();
        var hours = 0;
        diff = (cur - start) / 1000;
        min = Math.floor(diff / 60);
        sec = diff - (min * 60);

        if (min > 60) {
            hours = Math.floor(min / 60);
            var minutes = min % 60;
        }

        $scope.hours = hours;
        $scope.minutes = Math.round(min);
        $scope.seconds = Math.round(sec);
    }
    $interval(timer, 1000);

    $scope.openModal = function () {
        var modal = document.getElementById("goalModal");
        var closeX = document.getElementById("goalClose");

        modal.style.display = "block";

        closeX.onclick = function () {
            modal.style.display = "none";
        }

        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    $scope.bikeOneEndSessionFunction = function () {
        var socket = io.connect();

        socket.emit('bikeOneEndSession');
        console.log("Emit bikeOneEndSession");
    };

    socket.on('doneBikeOneSession', function () {
        console.log('Back to index.html');
        $window.location.href = "index.html";
    });

    // configure the bike1RPM chart 
    function config_bike1Chart() {
        var config = {
            type: 'line',
            data: {
                labels: [],
                datasets: [{
                    label: 'RPM',
                    backgroundColor: $window.chartColors.blue,
                    borderColor: $window.chartColors.blue,
                    data: [
                        0
                    ],
                    fill: false,
                }]
            },
            options: {
                maintainAspectRatio: false,
                responsive: true,
                title: {
                    display: true,
                    text: 'Bike One RPM'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'RPM'
                        }
                    }]
                }
            }
        };
        return config;
    }

});

