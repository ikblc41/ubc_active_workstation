"use strict";

var app = angular.module("App.controllers", []);

app.controller("bike2Controller", function ($scope, $interval, $window) {

    var socket = io.connect();
    var start = Date.now();
    var diff, min, sec = 0;

    // configure the RPM chart for the bike
    var config_bike2 = config_bike2Chart();

    // load the chart for the RPM
    $window.onload = function () {
        var ctx = document.getElementById('bike2RPMChart').getContext('2d');
        $window.bikeRPM = new Chart(ctx, config_bike2);
    };

    // listen for data with cadence tag, and assign it to
    socket.on("bike2rpm", function (rpm) {
        // console.log(data);
        $scope.$apply(function () {
            $scope.bike2RPMData = Math.round(rpm)
        });

        // add the new RPM data to the chart
        // try making the label more than one space so that datapoints dont get cut off
        config_bike2.data.labels.push("     ");
        config_bike2.data.datasets[0].data.push(Math.round(rpm));
        $window.bikeRPM.update();
    });

    socket.on("bike2speed", function (data) {
        // console.log(data);
        $scope.$apply(function () {
            $scope.bike2SpeedData = Math.round(data)
        });
    });

    socket.on("bike2distance", function (data) {
        // console.log(data);
        $scope.$apply(function () {
            $scope.bike2DistData = Math.round(data)
        });
    });

    socket.on("bike2calories", function (data) {
        // console.log(data);
        $scope.$apply(function () {
            $scope.bike2CalData = Math.round(data)
        });
    });

    var timer = function () {
        var cur = Date.now();
        var hours = 0;
        diff = (cur - start) / 1000;
        min = Math.floor(diff / 60);
        sec = diff - (min * 60);

        if (min > 60) {
            hours = Math.floor(min / 60);
            var minutes = min % 60;
        }

        $scope.hours = hours;
        $scope.minutes = Math.round(min);
        $scope.seconds = Math.round(sec);
    }
    $interval(timer, 1000);

    $scope.openModal = function () {
        var modal = document.getElementById("goalModal");
        var closeX = document.getElementById("goalClose");

        modal.style.display = "block";

        closeX.onclick = function () {
            modal.style.display = "none";
        }

        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    $scope.bikeTwoEndSessionFunction = function () {
        var socket = io.connect();
        socket.emit('bikeTwoEndSession');
        console.log("Emit bikeTwoEndSession");

    };

    socket.on('doneBikeTwoSession', function () {
        console.log('Back to index.html');
        $window.location.href = "index.html";
    });

    // configure the bike2RPM chart
    function config_bike2Chart() {
        var config = {
            type: 'line',
            data: {
                labels: [],
                datasets: [{
                    label: 'RPM',
                    backgroundColor: $window.chartColors.yellow,
                    borderColor: $window.chartColors.yellow,
                    data: [
                        0
                    ],
                    fill: false,
                }]
            },
            options: {
                maintainAspectRatio: false,
                responsive: true,
                title: {
                    display: true,
                    text: 'Bike Two RPM'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'RPM'
                        }
                    }]
                }
            }
        };
        return config;
    }

});
