"use strict";

var app = angular.module("App.controllers", []);

app.controller("statsController", function($scope, $window) {

	// date check
	var date = new Date(Date.now());
    var month = date.getMonth();

    var socket = io.connect();
    socket.emit("dateCheck");

	// want to make it so that we index into this array when choosing the x-axis value
	// for a data point... see "addData" event in line_chart_example
	var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 
                'September', 'October', 'November', 'December'];

    // get the colors that the charts will use (utils.js)
    var color = Chart.helpers.color;

	// LINE CHART CONFIG
	var config_bar = configBarChart();
	// AREA CHART CONFIG
	var config_area = configAreaChart();
	// PIE CHART CONFIG 
	var config_pie = configPieChart();

        socket.on("dateCheckFinished", function () {
		// line chart
	    var ctx_bar = document.getElementById('bar_chart').getContext('2d');
	    $window.myBar = new Chart(ctx_bar, config_bar);

	    // area chart
	    var ctx_area = document.getElementById('area_chart').getContext('2d');
		$window.myArea = new Chart(ctx_area, config_area);

		// testing getting total stats from the saved_data table instead
		socket.emit("getMonthlyStats");

		// pie chart
		var ctx_pie = document.getElementById('pie_chart').getContext('2d');
		$window.myPie = new Chart(ctx_pie, config_pie);
		socket.emit("getUseTimeStats");
	
	});

	socket.on("totalResults", function(results) {
		// dataset 0 is bike 1
		// dataset 1 is bike 2
		// dataset 2 is the total
		var i;

		console.log("bike 1 totals: " + results.bike1);
		console.log("bike 2 totals: " + results.bike2);
		console.log("combined totals: " + results.total);

		// add labels for the months to data
		for(i = 0; i < results.total.length; i++) {
			config_area.data.labels.push(MONTHS[i]);
		}

		config_area.data.datasets[0].data = results.bike1;
		config_area.data.datasets[1].data = results.bike2;
		config_area.data.datasets[2].data = results.total;
		
		window.myArea.update();

		// the server emits monthlyResults first, so everything will be updated now
		// can then ask for the current month distance and overwrite the current month data
		socket.emit("getCurMonthStats");

	});


	socket.on("monthlyResults", function(results) {
		console.log("monthly results: " + JSON.stringify(results));

		config_bar.data.datasets[0].data = results.bike1;
		console.log("bar dataset1: " + config_bar.data.datasets[0].data);
		config_bar.data.datasets[1].data = results.bike2;
		console.log("bar dataset2: " + config_bar.data.datasets[1].data);

		window.myBar.update();

	});

	socket.on("currentMonthStats", function(data) {			

		var bike1Dist = Number(config_area.data.datasets[0].data[month - 1]);
		var bike2Dist = Number(config_area.data.datasets[1].data[month - 1]);
		var totalDist = Number(config_area.data.datasets[2].data[month - 1]);
		console.log(bike1Dist + "\t" + bike2Dist + "\t" + totalDist);

		console.log(data.bike1 + "\t" + data.bike2);
		
		bike1Dist += Number(data.bike1);
		bike2Dist += Number(data.bike2);
		totalDist = bike1Dist + bike2Dist;
		// area graph
		config_area.data.labels.push(MONTHS[month]);

		config_area.data.datasets[0].data.push(bike1Dist.toFixed(3));
		config_area.data.datasets[1].data.push(bike2Dist.toFixed(3));
		config_area.data.datasets[2].data.push(totalDist.toFixed(3));

                window.myArea.update();

		// bar graph
		config_bar.data.datasets[0].data[month] = data.bike1;
		config_bar.data.datasets[1].data[month] = data.bike2;

		window.myBar.update();
	});

	socket.on("useTimeResults", function(results) {
		console.log("use time results: " + results);

		config_pie.data.datasets[0].data = results;
		console.log(config_pie.data.datasets[0].data);

		window.myPie.update();

	});

	/*
	 * Configure the bar chart object. 
	 * Datasets, labels and interactive components are all specified here.
	 * Return: a bar chart object
	 */
	function configBarChart() {
		var config = {
		    type: 'bar',
		    data: {
				// will always show data for one full year at a time
				// as the year progresses there will be more data points and then will reset at the start of the next year
		        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 
                'September', 'October', 'November', 'December'],
		        datasets: [{
		            label: 'Bike 1 Distance',
		            backgroundColor: color($window.chartColors.blue).alpha(0.5).rgbString(),
		            borderColor: $window.chartColors.blue,
		            borderWidth: 1,
		            data: [0,0,0,0,0,0,0,0,0,0,0,0],
		            fill: false,
		        }, 
		        {
		            label: 'Bike 2 Distance',
		            backgroundColor: color($window.chartColors.yellow).alpha(0.5).rgbString(),
		            borderColor: $window.chartColors.yellow,
		            borderWidth: 1,
		            data: [0,0,0,0,0,0,0,0,0,0,0,0],
		            fill: false,
		        }]
		    },
		    options: {
		        responsive: true,
		        title: {
		            display: true,
		            text: 'Distance Pedalled per Bike'
		        },
		        tooltips: {
		            mode: 'index',
		            intersect: false,
		        },
		        hover: {
		            mode: 'nearest',
		            intersect: true
		        },
		        scales: {
		            xAxes: [{
		                display: true,
		                scaleLabel: {
		                    display: true,
		                    labelString: 'Month'
		                }
		            }],
		            yAxes: [{
		                display: true,
		                scaleLabel: {
		                    display: true,
		                    // format the distance to be km... more applicable in larger scale
		                    labelString: 'Distance (km)'
		                }
		            }]
		        }
		    }
		};
		return config;
	}

	/*
	 * Configure the area chart. 
	 * Datasets, labels and interactive components are all specified here
	 * Return: an area chart object
	 */
	function configAreaChart() {
		var config = {
		    type: 'line',
		    data: {
		    	// currently in march... no data for january or february so set labels and data accordingly
		        labels: [],
		        // labels: MONTHS,
		        datasets: [{
		            label: "Total Distance Bike 1",
		            backgroundColor: $window.chartColors.blue,
		            borderColor: $window.chartColors.blue,
		            // no data for January and February, add March totals
                    data: [
                    	0,
                    	0 
		            ],
		            fill: false,
		        }, {
		            label: "Total Distance Bike 2",
		            backgroundColor: $window.chartColors.yellow,
		            borderColor: $window.chartColors.yellow,
		            // no data for January and February, add March totals
		            data: [
		            	0,
		            	0
		            ],
		            fill: false,
		        }, {
		            label: "Total Distance",
		            backgroundColor: color($window.chartColors.red).alpha(0.5).rgbString(),
		            borderColor: $window.chartColors.red,
		            // no data for January and February, add March totals
                    data: [
		                0,
                        0 
		            ],
		            fill: "start",
		        }]
		    },
		    options: {
		        responsive: true,
		        title: {
		            display: true,
		            // also want to format to km here
		            text: 'Total Distance of Workstation to Date'
		        },
		        tooltips: {
		            mode: 'index',
		            intersect: false,
		        },
		        hover: {
		            mode: 'nearest',
		            intersect: true
		        },
		        scales: {
		            xAxes: [{
		                display: true,
		                scaleLabel: {
		                    display: true,
		                    labelString: 'Month'
		                }
		            }],
		            yAxes: [{
		                display: true,
		                scaleLabel: {
		                    display: true,
		                    labelString: 'Distance (km)'
		                }
		            }]
		        }
		    }
		};
		return config;
	}

	/*
	 * Configure the pie chart. 
	 * Datasets, labels and interactive components are all specified here.
	 * Return: a pie chart object
	 */
	function configPieChart() {
		var config = {
			type: 'pie',
			data: {
				datasets: [{
					data: [0,0,0,0],
					backgroundColor: [
						$window.chartColors.red,
						$window.chartColors.orange,
						$window.chartColors.yellow,
						$window.chartColors.green,
					],
					label: 'Time'
				}],
				labels: [
					'Under 30 min',
					'30 min to 1 hour',
					'1 hour to 2 hours',
					'Over 2 hours'
				]
			},
			options: {
				responsive: true,
				title: {
		            display: true,
		            text: 'Duration of Use Breakdown'
		        }
			}
		};
		return config;
	}
});
