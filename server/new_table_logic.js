var startFlag = 0;
var date;
var month;
var year;

socket.on("dateCheck", function() {
	if (startFlag ==0) {
		// server.js has just started or been restarted
		// get the date information
		date = new Date(Date.now());
		month = date.getMonth();
		year = date.getFullYear();
		startFlag = 1;
	}

	// this will execute everytime the stats page has been loaded/visited
	var curDate = new Date(Date.now());
	// want these to be able to compare to when this file was first started
	var curMonth = curDate.getMonth();
	var curYear = curDate.getFullYear();

	if (curYear != year) {
		// create two new entries, one for each with year being the curYear
		year = curYear;
		// create the entries here	
		con.query("INSERT INTO saved_data (Bike, January, February, March, April, May, June, July, August, September, October, November, December, Year," 
			+ " useTime_u30, useTime30_60, useTime60_120, useTime_o120) " 
			+ " VALUES ('BikeOne', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " + curYear + ", 0, 0, 0, 0)", function(err, result) {
				if(err) {
					console.log("INSERT ERROR: " + err);
				} else {
					console.log("New record added successfully");
				}
		});

		con.query("INSERT INTO saved_data (Bike, January, February, March, April, May, June, July, August, September, October, November, December, Year," 
			+ " useTime_u30, useTime30_60, useTime60_120, useTime_o120) " 
			+ " VALUES ('BikeTwo', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, " + curYear + ", 0, 0, 0, 0)", function(err, result) {
				if(err) {
					console.log("INSERT ERROR: " + err);
				} else {
					console.log("New record added successfully");
				}
		});

	}

	if (curMonth != month) {
		// if we are in a new month then need to write to the database and update client side
		// set the month to be the new month
		month = curMonth;
		updateGraphData(socket, curMonth, curYear);
	}

});


socket.on("getMonthlyStats", function() {

	var results = [];
	// get all months from the saved_data table for each bike
    con.query("SELECT January, February, March, April
    	May, June, July, August, September, October, November, 
    	December FROM saved_data WHERE Bike = bike1 AND Year = " + curYear. function(err, rows) {
    		if(rows[0] == null) {
    			socket.emit("bikeResults", -1);
    			return;
    		} else {
    			// should get an array back
    			results.push(rows[0]);
    		}
    });

    con.query("SELECT January, February, March, April
    	May, June, July, August, September, October, November, 
    	December FROM saved_data WHERE Bike = bike2 AND Year = " + curYear. function(err, rows) {
    		if(rows[0] == null) {
    			socket.emit("bikeResults", -1);
    			return;
    		} else {
    			// should get an array back
    			results.push(rows[0]);
    			// emit here so that both bike objects data have been included
    			socket.emit("monthlyResults", results);
    		}
    });
});

socket.on("getTotalStats", function() {

	var results = [];
	// mimic after getUserStats but with different request
    con.query("SELECT TotalDistance FROM stats WHERE StudentID = " + bike1ID, function(err, rows) {
        if (rows[0] == null) {
            console.log("No monthly data for bike 1");
            return;
        } else {
	       results.push(rows[0]);
    	}
    });
    con.query("SELECT TotalDistance FROM stats WHERE StudentID = " + bike2ID, function(err, rows) {
        if(rows[0] == null) {
            console.log("No monthly data for bike 2");
            return;
        } else {
            results.push(rows[0]);
            // emit here so that both bike objects data have been included
            socket.emit("totalResults", results);
        }	
    });
});


function updateGraphData(socket, curMonth, curYear) {
	console.log("UPDATING GRAPH DATA");

	// array for the months so that we can convert integer to string
	var MONTHS = ["January". "February", "March", "April", "May", "June", "July",
			"August", "September", "October", "November", "December"];

	// query the database here and emit stats 
	// the monthly stats column will be from the previous month and not reset yet
	var updates = [];

	// mimic after getUserStats but with different request
    con.query("SELECT * FROM stats WHERE StudentID = " + bike1ID, function(err, rows) {
        if (rows[0] == null) {
            console.log("No monthly data for bike 1");
            return;
        } else {
	       updates.push(rows[0]);
    	}
    });
    con.query("SELECT * FROM stats WHERE StudentID = " + bike2ID, function(err, rows) {
        if(rows[0] == null) {
            console.log("No monthly data for bike 2");
            return;
        } else {
            updates.push(rows[0]);
        }	
    });

    // have the data from the stats table now in updates... can clear the monthly data now
    con.query("UPDATE stats SET MonthDistance = " + 0
    	+ ", MonthCalories = " + 0 + ", MonthTime = " + 0 +
    	"WHERE StudentID = " + bike1ID, function(err, rows) {
    		console.log(err);
    });
    con.query("UPDATE stats SET MonthDistance = " + 0
    	+ ", MonthCalories = " + 0 + ", MonthTime = " + 0 +
    	"WHERE StudentID = " + bike2ID, function(err, rows) {
    		console.log(err);
    });

    // we are here because the month has changed, but we want to write the data for the previous month
    if (curMonth == 0) {
    	// January to December
    	curMonth = 11;
    } else {
    	curMonth = curMonth - 1;
    }
    // write monthly totals to new database, divide by 1000 to get kms
    // bike 1
    con.query("UPDATE saved_data SET " + MONTHS[curMonth] + " = " + updates[0].MonthDistance / 1000
    	+ " WHERE Year = " + curYear 
    	+ " AND Bike = bike1", function(err, rows) {
    	console.log(err);
    });
    // bike 2
    con.query("UPDATE saved_data SET " + MONTHS[curMonth] + " = " + updates[1].MonthDistance / 1000
    	+ " WHERE Year = " + curYear 
    	+ " AND Bike = bike2", function(err, rows) {
    	console.log(err);
    });    	
}

