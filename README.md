# UBC Team 41 Active Workstation Repository

## \workstation
Contains files that read the cadence sensor, and must be run locally in the workstation computer.

## \server
Contains the website files that display on the url [cycle.ikblc.ubc.ca](cycle.ikblc.ubc.ca)

## \tweeter
Contains the tweeter app that updates the Twitter social media feed

## .gitignore
Specify any files you don't want to include in the repository in this file.