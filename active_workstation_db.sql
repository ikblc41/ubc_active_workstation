-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: active_workstation
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `saved_data`
--

DROP TABLE IF EXISTS `saved_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_data` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Bike` varchar(32) DEFAULT NULL,
  `January` varchar(32) DEFAULT NULL,
  `February` varchar(32) DEFAULT NULL,
  `March` varchar(32) DEFAULT NULL,
  `April` varchar(32) DEFAULT NULL,
  `May` varchar(32) DEFAULT NULL,
  `June` varchar(32) DEFAULT NULL,
  `July` varchar(32) DEFAULT NULL,
  `August` varchar(32) DEFAULT NULL,
  `September` varchar(32) DEFAULT NULL,
  `October` varchar(32) DEFAULT NULL,
  `November` varchar(32) DEFAULT NULL,
  `December` varchar(32) DEFAULT NULL,
  `Year` int(32) DEFAULT NULL,
  `useTime_u30` int(32) DEFAULT NULL,
  `useTime30_60` int(32) DEFAULT NULL,
  `useTime60_120` int(32) DEFAULT NULL,
  `useTime_o120` int(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_data`
--

LOCK TABLES `saved_data` WRITE;
/*!40000 ALTER TABLE `saved_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stats` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CwlUsername` varchar(32) NOT NULL,
  `StudentID` int(32) DEFAULT NULL,
  `TotalCalories` int(11) DEFAULT NULL,
  `TotalDistance` int(11) DEFAULT NULL,
  `TotalTime` int(11) DEFAULT NULL,
  `LastSessionCalories` int(11) DEFAULT NULL,
  `LastSessionDistance` int(11) DEFAULT NULL,
  `LastSessionTime` int(11) DEFAULT NULL,
  `MonthCalories` int(11) DEFAULT NULL,
  `MonthDistance` int(11) DEFAULT NULL,
  `MonthTime` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats`
--

LOCK TABLES `stats` WRITE;
/*!40000 ALTER TABLE `stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `stats` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-12 13:39:17
