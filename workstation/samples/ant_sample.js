'use strict';

const Ant = require('./ant-plus/ant-plus');
const stick = new Ant.GarminStick2();
const cadenceSensor = new Ant.CadenceSensor(stick);

cadenceSensor.on('cadenceData', data => {
  console.log(`cadence: ${data.CalculatedCadence}`);
});
//`cadence: ${data.CalculatedCadence}`
//+ `distance: ${data.CalculatedDistance}`

stick.on('startup', function () {
	console.log('startup');
	cadenceSensor.attach(0, 0);
	console.log('attached');
});

if (!stick.open()) {
	console.log('Stick not found!');
}
else
	console.log('Stick Attached');