//===================================================
// workstation.js
// Author: 2017-2018 CPEN 491 Team 41
// Description: This app reads the Ant+ cadence sensors
//   attached to the bike workstation and transmits
//   the RPM data to the Team 41 web server for display
//===================================================

'use strict';

var express = require('express');
var app = express();
var http = require('http').Server(app);
var path = require('path');
var io = require('socket.io')(http);
var ioClient = require('socket.io-client');

var client = ioClient.connect('https://ubcactiveworkstation.ca');

const Ant = require('./ant-plus/ant-plus');
const stick = new Ant.GarminStick2();
const bike1Sensor = new Ant.CadenceSensor(stick);
const bike2Sensor = new Ant.CadenceSensor(stick);
const bike1SensorID = 21403;
const bike2SensorID = 5555; //555

var prevBike1Cadence = 0;
var prevBike2Cadence = 0;

var counter = 0;
var seconds = 10;
var state = 0;

// ---------------
// DEBUG MODE
// Set to 0 to send cadence sensor values
// Set to 1 to send counter values
// ---------------
var debugMode = 0;

if(debugMode == 0)
{
	bike1Sensor.on('cadenceData', data => {
		if(prevBike1Cadence > 0)
		{
			client.emit('bike1Cadence', data.CalculatedCadence);
			console.log('Bike 1 cadence reading: ' + data.CalculatedCadence + ' from Device ID: ' + data.DeviceID);
		}

		prevBike1Cadence = data.CalculatedCadence;
	});

	bike2Sensor.on('cadenceData', data => {
		if(prevBike2Cadence > 0)
		{
			client.emit('bike2Cadence', data.CalculatedCadence);
			console.log('Bike 2 cadence reading: ' + data.CalculatedCadence + ' from Device ID: ' + data.DeviceID);
		}

		prevBike2Cadence = data.CalculatedCadence;
	});

	stick.on('startup', function () {
		bike1Sensor.attach(0, bike1SensorID);
		bike2Sensor.attach(1, bike2SensorID);
	});

	if (!stick.open()) {
		console.log('Stick not found!');
	}
}

else if(debugMode == 1)
{
	var intervalA = setInterval(function () {

		counter++;
		if(counter < 2)
		{
            client.emit('bike1Cadence', 0);
            client.emit('bike2Cadence', 0);
            console.log('Emit 0 for ' + counter + ' instance');
		}

		if( (counter >= 2) && (counter < 7) )
		{
            //client.emit('bike1Cadence', 200);
            client.emit('bike2Cadence', 200);
            console.log('Emit 200 for ' + counter + ' instance');
		}

		if( counter >= 7)
		{
			counter = 0;
		}
	}, (1000));
}

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname, '../', 'client_test.html'));
});

http.listen(3000, () => console.log("Listening on port: 3000"));
